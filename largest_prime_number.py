def finding_prime_factors(input_number):
    highest_prime_factor = 1
    highest_divider = 2

    while highest_divider <= input_number / highest_divider:
        if input_number % highest_divider == 0:
            highest_prime_factor = highest_divider
            input_number = input_number / highest_divider #We basically divise our input number to 
        else:
            highest_divider += 1

    if highest_prime_factor < input_number:
        highest_prime_factor = input_number

    return highest_prime_factor

print(finding_prime_factors(600851475143))